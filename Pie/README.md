
# Pie Chart

- Slice label is displayed when hover over slice

![alt text](https://0nq1tw.am.files.1drv.com/y4mpV4afN1ei0E37sKfwlRT42d0ZzknWpVyFTA8DPqiooTV-MCb8pM2u-skeDMbDRdLgooC560N7skmIdBk7Q2PWI5kpgikrZfHVA31x-t5DktOGOt8goZ2wHaj5NekvvUC2F2x5fPiT6vH43jUAYIfAh-0n2gS7aDuinWTckRoswD_KFYZUTWnc2nqtUN8FJz1HDJLzmIAQaxaeNpbVLBvRQ?width=622&height=416&cropmode=none "Pie chart")

## JSFiddle
Pie (d3.js version 3) also in [JSFiddle](https://jsfiddle.net/sallakupiainen/1wtgs69o/).